
var page = require('webpage').create();
var expectedContent = '<html><body><span id="pie">Loading..</span></body></html>';
var expectedLocation = 'http://localhost/test-pie-01.html';

page.viewportSize = { width: 320, height: 200 };
page.onConsoleMessage = function(msg) {
    console.log(msg);
};

console.log("setContent");
page.setContent(expectedContent, expectedLocation);

page.open(expectedLocation, function (status) {
    console.log("open status: "+status);
    if (status == 'success') {
	console.log('inject jQuery');
	page.injectJs("/usr/share/javascript/jquery/jquery.js");

	console.log('inject jQuery.sparkline');
	page.injectJs("/usr/share/javascript/jquery.sparkline/jquery.sparkline.js");
	page.evaluate(function () {
	    console.log('evaluate');
	    $('#pie').sparkline([1,2,3,4,5,4,3,2,1], { type:'pie' });
	    console.log($('#pie'));
	});

	page.render('test-pie-01.jpg', {format: 'jpeg', quality: '90'});

        setTimeout(function() {
            phantom.exit(0);
        }, 10);
    }
    setTimeout(function() {
        phantom.exit(1);
    }, 20);
});
